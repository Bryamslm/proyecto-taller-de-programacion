import sys
import requests
import json
import cognitive_face as CF
from PIL import Image, ImageDraw, ImageFont
import math

def main():
    '''In this function the other functions are called and the menu is printed'''
    try:
        rostros, picture=rostro()
                                    #If the image path is wrong or there are no faces, 
                                    #the rostro() function is called again
        if len(rostros) == 0:

            print('\nNo se detectaron rostros en la imagen, inténtelo nuevaente')

            main()

    except:
        print('\nLo siento, la dirección de la imagen es errónea,'
            'por favor intentelo nuevamente\n')

        main()

    historial= historial_consultas(rostros) #History is saved

    while True:

        print('\n\n\t\t\t\t\t------------- ¡Hola! Bienvenido al'
            'menú de consultas -------------')

        print('\na- Mostrar la cantidad de rostros en la imagen.')

        print('\nb- Mostrar lista de las edades de todas las personas'
            'y la más joven.')

        print('\nc- Mostrar edad y género (inserción).')

        print('\nd- Mostrar edad y género en rango de 10 (inserción).')

        print('\ne- Mostrar edad y promedio de edades.')

        print('\nf- Mostrar edad y género, separados por género (quicksort).')

        print('\ng- Mostrar faceId, edad y género (quicksort).')

        print('\nh- Mostrar faceId y el color del cabello.')

        print('\ni- Mostrar faceId y emociones de las personas.')

        print('\nj- Mostrar los accesorios de las personas.')

        print('\nk- Mostrar rectángulo en el rostro de la o las personas.')

        print('\nl- Mostrar la persona que tenga el porcentaje'
            ' de felicidad más alto.')

        print("\nm- Mostar la felicidad mayor, debajo del promedio.")

        print("\nn- Mostar la persona con más barba y que está más feliz.")

        print("\no- Mostrar la persona que usa lentes y está más enojada.")

        print("\np- Mostrar la mujer más rubia.")

        print('\nq- Consultas propuestas:\n\tq1- Mostrar persona con la mayor'
            ' sonrisa\n\tq2- Mostrar persona con más bigote.')

        print('\nr- Historial de consultas:\n\tr1- Mostrar persona más joven'
            ', emociones e imagen con rectángulo en la cara'
            '\n\tr2- Mostrar el promedio de emociones'
            '\n\tr3- Mostrar historial de consultas')

        print('\n0- ¿Quieres salir? Presiana 0 para hacerlo.')

        print('\n9- ¿Quieres cambiar la imagen actual a otra? Presiana 9.')

        opcion=input('\nIngrese la opción de la consulta que desea realizar: ')

        print('------------------------------------------------------------------------------')
        if opcion== 'a':

            mostrar_cantidad_rostros(rostros)

        elif opcion=="b":
            mostar_lista_y_menor(rostros)

        elif opcion=='c':
            mostrar_edad_genero(rostros)

        elif opcion=='d':
            mostrar_edad_genero_rango(rostros)

        elif opcion=='e':
            mostrar_edad_y_promedio(rostros)

        elif opcion=='f':
            mostrar_edad_genero_quick(rostros)

        elif opcion=='g':
            mostrar_id_edad_genero(rostros)

        elif opcion=='h':
            mostrar_faceId_y_color_de_cabello(rostros)

        elif opcion=='i':
            mostrar_id_emociones(rostros)

        elif opcion=='j':
            mostrar_accesorios(rostros)

        elif opcion=='k':
            show_image(picture, historial)

        elif opcion=='l':
            mostrar_más_felicidad(rostros)

        elif opcion=='n':
            mostrar_barba_felicidad(rostros)

        elif opcion=='m':
            mostrar_felicidad_promedio(rostros)

        elif opcion=='p':
            rubia_mujer(rostros)

        elif opcion=='q1':
            mostrar_sonrisa(rostros)
                                        #the function is called according to the query to be made
        elif opcion=='q2':
            mostrar_bigote(rostros)

        elif opcion=='o':
            lentes_enojo(rostros)

        elif opcion=='r1':
            joven_emocionnes_rec(picture, historial)

        elif opcion=='r2':
            mostrar_prom_emotion(historial)

        elif opcion=='r3':
            print(historial)

        elif opcion== '0':

            exit()

        elif opcion=='9':

            main()

        else:
            print('\nLo siento, usted a presionado una opción inválida\n')

            continue

        print('------------------------------------------------------------------------------')

        while True:

            opcion=input('\nPresione 9 para volver al menú principal ó 0 para salir: ')

            if opcion=='9':

                break          #The user is asked if they want to return to the menu or exit

            elif opcion=='0':
                exit()

            else:

                print('\nLo siento, usted a presionado una opción inválida\n')

                continue
#===========================================================================================================

#0

def rostro():

    '''
    Function that connects with the facial recognition service to analyze an image with faces
    Receive: nothing
    Returns: list of faces with their characteristics 
    '''
    #c:/Users/bryam/Desktop/TEC/Segundo_Semestre/Taller/fotos/reunion.jpg
    SUBSCRIPTION_KEY = 'd691bbd350ab47948e784cb8df0e72d8'
    BASE_URL = 'https://taller-progra-2020.cognitiveservices.azure.com/face/v1.0/'
    CF.BaseUrl.set(BASE_URL)
    CF.Key.set(SUBSCRIPTION_KEY)

    #Obtener las emociones de una persona
    #picture recibe la foto de la persona que se desea obtener las emociones
    #headers = {'Ocp-Apim-Subscription-Key': 'e70e11c9cb684f21b8b37313fd60e5bc'}
    image_path = input("Introduzca el path de la imagen: ")
    #https://docs.microsoft.com/en-us/azure/cognitive-services/computer-vision/quickstarts/python-disk
    # Read the image into a byte array
    image_data = open(image_path, "rb").read()
    headers = {'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
    'Content-Type': 'application/octet-stream'}
    params = {
        'returnFaceId': 'true',
        'returnFaceLandmarks': 'false',
        'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,'
        'emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
    }
    response = requests.post(
                             BASE_URL + "detect/", headers=headers, params=params, data=image_data)
    analysis = response.json()

    return analysis,image_path


 #===========================================================================================================   

#1. (A)

def mostrar_cantidad_rostros(rostros):
    '''
    Function that shows the number of faces, once the action is executed it returns to the query menu
    Receive: face list
    Returns: nothing
    '''
    cantidad= len(rostros)    #variable

    print('\nLa cantidad de rostros detectadas es de:', cantidad)    #print


 #===========================================================================================================   

 #2. (C)

def mostrar_edad_genero(rostros):
    '''
    Function that shows the age and gender of faces from youngest to oldest using the insertion method.
    Receive: face list
    Returns: nothing
    '''
    edades_generos=list()  #Ages and genders will be saved in a messy way.

    edades_generos_orden=list() # ages and genders will be stored in an orderly manner.

    for rostro in rostros:

        temporal=list()

        faceAttributes=rostro['faceAttributes']

        temporal.append(faceAttributes['age'])       #cycle to add age and gender to age_gender list.
        temporal.append(faceAttributes['gender'])

        edades_generos.append(temporal)

    while len(edades_generos) != 0:

        edad_mayor= 0                   #cycle to sort ages and genders by insertion.

        temporal_2=list()

        for edad in edades_generos:

            if edad[0] > edad_mayor:

                edad_mayor=edad[0]      #cycle saved by the oldest person.

                temporal_2=edad

        
        edades_generos_orden.insert(0, temporal_2)  #add the oldest person to the first list position in order.
                                                    # at the end, it is ordered from minor to major according to age.
        edades_generos.remove(temporal_2)   # age and gender are removed added to the list in order of the unordered list.
    print('la lista de edades y géneros (inserción) es:\n')

    print(edades_generos_orden)   


 #===========================================================================================================   

 #3. (D)
    
def mostrar_edad_genero_rango(rostros):
    '''
    Prints a list of ages and genders in sublists within a list, separated by an age range of 10
    Receive: face list
    Returns: nothing
    '''
    edades_generos=list()   #Ages and genders will be saved in a messy way.

    edad_genero_rango=list()  # ages and genders will be saved in ranges of 10, starting from 0 to 10 and so on.

    edad_genero_rango_completo=list() # each list will be saved with its respective ranks from lowest to highest.

    for rostro in rostros:

        temporal=list()

        faceAttributes=rostro['faceAttributes']     #cycle to add age and gender to age_gender list.

        temporal.append(faceAttributes['age'])

        temporal.append(faceAttributes['gender'])

        edades_generos.append(temporal)

    asignar_contador=True

    while len(edades_generos) != 0:

        edad_mayor= 0            #cycle to sort ages and genders by insertion.

        temporal_2=list()

        for edad in edades_generos:

            if edad[0] > edad_mayor:

                edad_mayor=edad[0]       #cycle saved by the youngest person.

                temporal_2=edad

        if asignar_contador==True:

            residuo=edad_mayor%10

            suma=10-residuo

            contador=edad_mayor+suma

            asignar_contador=False

        if contador==0:
            break

        elif edad_mayor >= contador-10:

            edad_genero_rango.insert(0, temporal_2)    #if the age is less than the specified range, it will be added to the corresponding range.

            edades_generos.remove(temporal_2)
        else:
            edad_genero_rango_completo.insert(0, edad_genero_rango)

            edad_genero_rango=list()

            edad_genero_rango.insert(0, temporal_2)    #if age is greater than the range, the list with the range is added to the list with
                                                    #the ordered ranges also the range list is cleaned and the age is added.
            edades_generos.remove(temporal_2)

            residuo=edad_mayor%10

            suma=10-residuo

            contador=edad_mayor+suma

    if len(edad_genero_rango) != 0:
        edad_genero_rango_completo.insert(0, edad_genero_rango)
                
    print('La lista de edades y géneros en rago de 10 (inserción) es:\n')

    print(edad_genero_rango_completo)   #prints


#=========================================================================================================== 

#4. (G)

def mostrar_id_edad_genero(rostros):
    '''
    Print a list of face Id, ages and genders in sub-lists, ordered from smallest to largest by age.
    Receive: face list
    Returns: nothing
    '''

    ids_edades_rostros=list()

    for rostro in rostros:
        temporal=dict()                        # query segment id, age, gender via quicksort.

        faceId=rostro['faceId']
        faceAttributes=rostro['faceAttributes']
        edad=faceAttributes['age']
        genero=faceAttributes['gender']

        temporal['faceId']= faceId
        temporal['age']= edad
        temporal['gender']= genero

        ids_edades_rostros.append(temporal)

    lista=ids_edades_rostros                         # query segment id, age, gender via quicksort.

    lista= quicksort_1(lista)

    lista= quita_dicc(lista, True)

    print('La lista con sublistas de faceId, edad y género (quicksort) es:\n')

    print(lista)                          

#===========================================================================================================

#7. (I)

def mostrar_id_emociones(rostros):
    '''
    Function that prints the FaceId and emotions of each person
    Receive: face list
    Returns: nothing
    '''

    print('Los faceId y emociones de las personas son:\n')

    for rostro in rostros:

        faceId=rostro['faceId']

        faceAttributes=rostro['faceAttributes']  
                                                #The faceId and emotions of each person are extracted
        emotion=faceAttributes['emotion']

        print('\nfaceId:', faceId)
  
        print('\nEmociones:')

        for x, y in emotion.items():
                                        #Emotions are printed
            print('\t',x
            + ':', y) 

#==================================================================================================================

#8. (F)

def  mostrar_edad_genero_quick(rostros):
    '''
    Ages and genders are printed separated by gender and sorted using quicksort
    Receive: face list
    Returns: nothing
    '''

    edades_rostros=list()

    for rostro in rostros:
        temporal=dict()                       

        faceAttributes=rostro['faceAttributes']
        edad=faceAttributes['age']
        genero=faceAttributes['gender']    #Age and gender are added to a dictionary, 
                                           #at the same time that dictionary is listed according to gender

        temporal['age']= edad
        temporal['gender']= genero

        edades_rostros.append(temporal)

    hombres=list()
    mujeres=list()

    for genero in edades_rostros:

        if genero['gender']=='female':

            mujeres.append(genero)
        else:
            hombres.append(genero)

    mujeres= quicksort_1(mujeres) #The sort function is called
    hombres= quicksort_1(hombres) #The sort function is called

    mujeres=quita_dicc(mujeres, False) #This function is called that
                                       #passes the age and gender from dictionary to list
    hombres=quita_dicc(hombres, False)

    hombres_mujeres=list()
    
    if len(mujeres) != 0:
        hombres_mujeres.append(mujeres)
                                        #The 2 lists are put into 1
    if len(hombres) != 0:
        hombres_mujeres.append(hombres)
    print('La lista de edades y géneros, separados por género (quicksort) es:\n')
    print(hombres_mujeres)

def quita_dicc(lista, bol):
    '''
    Function that changes passing items from dictionaries to lists
    Receive: List with dictionaries inside and a boolean, True or False
    Returns: List with sublists
    '''
    pos=0

    for i in lista:
        temp=list()

        if bol == True:
            faceId=i['faceId']  #If the received boolean is True, the faceId of each dictionary is also matched
            temp.append(faceId)

        edad=i['age']
        genero=i['gender']

        temp.append(edad)
        temp.append(genero)

        lista[pos]= temp
        pos+=1

    return lista
#===========================================================================================================

#9. 

def particionado_1(lista):
    '''
    Receive a list with dictionaries inside
    take an age pivot to add the ages to a list if they are less 
    than the pivot or to another list if the ages are greater than the pivot
    Receive: List with dictionaries inside
    Returns: List with sublists or dictionaries
    '''
    menores= list()
    mayores= list()
    saca_pivote= lista[-1] #The dictionary is taken from where the pivot will be taken

    pivote= saca_pivote['age'] #the pivot is taken out of the dictionary

    for x in lista[:-1]:

        if x['age'] < pivote:
                                #if the age of the dictionary x is less than the pivot, 
                                #the dictionary is entered into the list of minors, if not, to the older list
            menores.append(x)  
        else:
            mayores.append(x)

    return [menores, saca_pivote, mayores]
#===========================================================================================================  

#10.

def quicksort_1(lista):
    '''
    Function that makes sure the list is sorted correctly
    Receive: List with dictionaries inside messy
    Returns: List with dictionaries organized
    '''

    if len(lista) > 0:

        lista= particionado_1(lista) #The function is called to partition the list

        cont= 0

        while cont < len(lista):

            elemento= lista[cont]

            if type(elemento) == list:
                if len(elemento) == 0:      #If the type of the element is list, it is checked and 
                                            #if it is an empty list, it is eliminated, if it is a 
                    del lista[cont]         #list with only 1 dictionary inside, the list is replaced 
                elif len(elemento) == 1:    #by the dictionary, if it is not called the partition partition_1
                    lista[cont]= elemento[0]
                else:
                    lista=lista[:cont]+ particionado_1(elemento)+ lista[cont+1:]
            else:
                cont+= 1
        return lista


#===========================================================================================================

#11. (B)

def mostar_lista_y_menor(rostros):
    '''
    Shows the ages in a list and the youngest age
    Receive: faces list
    Returns: nothing
    '''
    
    edades =list()           #variables
    edad_menor = 120


    for rostro in rostros:
        faceAttributes = rostro["faceAttributes"]
        age = faceAttributes["age"]              #cycle that brings up the list of ages.
        edades.append(age)

        if age < edad_menor:
            edad_menor = age                     #cycle that brings up the youngest age.
    print("\nLa lista de edades es:", edades)

    print("\nLa edad menor es de", edad_menor)  


#===========================================================================================================
     
#12. (H)

def mostrar_faceId_y_color_de_cabello(rostros):
    '''
    Show Face Id and hair color of each person
    Receive: faces list
    Returns: nothing
    '''

    print('Los faceIds y el color de cabello de las personas son:\n')

    for rostro in rostros:

        faceId = rostro['faceId']

        faceAttributes = rostro['faceAttributes']

        hair = faceAttributes['hair']

        hairColor = hair['hairColor']

        if len(hairColor) > 0:      #It is verified that the person's hair color has been detected

            color = hairColor[0]        

            color1 = color['color']

        else:
            color1 = '¡No se detecto color de cabello en esta persona!'      
                                                                        

        print("\nEl faceId es: ", faceId)

        print("El color del cabello es: ",color1)   #prints


#===========================================================================================================

#13 (D)

def mostrar_edad_y_promedio(rostros):
    '''
    shows list of ages and average ages
    Receive: faces list
    Returns: nothing
    '''
    temporal = 0                                #variables
    edad_promedio=list()

    for rostro in rostros:
        faceAttributes = rostro["faceAttributes"]         #cycle that takes age and average
        age = faceAttributes["age"]
        edad_promedio.append(age)
        temporal= temporal+age
    print("\nLa lista de edades es: ", edad_promedio)

    cantidad=len(edad_promedio)                            #length of average_quantity
    promedio= temporal // cantidad
    print("\nEl promedio de las edades es: ", promedio)   


#===========================================================================================================

#14. (J)

def mostrar_accesorios(rostros):
    '''
    Shows the accessories of each person,
    if a person does not carry accessories indicates it
    Receive: faces list
    Returns: nothing
    '''

    print('\nA continuación el faceId y accesorios de cada persona:')

    for rostro in rostros:
        
        faceId=rostro['faceId']
        faceAttributes=rostro['faceAttributes']
        accessories=faceAttributes['accessories']   #cycle to display accessories

        print("\nFaceId:",faceId)  #prints

        print('\nAccesorios:')

        if len(accessories)== 0: #verify that the person carries accessories
            print("\tLo siento, la persona no porta accesorios")

        else:
   
            for acce in accessories:
                print('\t', acce['type'])


#===========================================================================================================

#15. (L)

def mostrar_más_felicidad(rostros):
    '''
    Shows the Face Id and
    happiness confidence of the happiest person
    Receive: faces list
    Returns: nothing
    '''
    faceId_real = " "
    happiness_mayor = 0                                     #variables

    for rostro in rostros:
        faceId=rostro['faceId']
        faceAttributes = rostro['faceAttributes']       # cycle that brings out the most happiness
        emotion = faceAttributes['emotion']  
        happiness=emotion['happiness']

        if happiness > happiness_mayor:
            happiness_mayor = happiness           ##cycle that brings out happiness
            faceId_real= faceId
    print("\nLa persona con la mayor felicidad es:", faceId_real,
     "\ncon una puntuación de: ", happiness_mayor)    #print


#===========================================================================================================

#16 (N)

def mostrar_barba_felicidad(rostros):
    '''
    Show the person with the most beard and happiness
    Receive: faces list
    Returns: nothing
    '''
    hombre = 'male'
    beard = 0                                                           #variables
    estado=False
    happiness1 = 0
    faceId1 = ""

    for rostro in rostros:
        faceId=rostro['faceId']
        faceAttributes = rostro['faceAttributes']
        facialHair = faceAttributes['facialHair']
        gender= faceAttributes['gender']                                # cycle to remove beard and happiness
        barba=facialHair['beard']
        emotion = faceAttributes['emotion']  
        happiness=emotion['happiness']

        if gender== hombre:
            estado= True

            if barba >= beard: 

                beard=barba
                happiness1 = happiness                           #cycle that brings out happiness
                faceId1 = faceId                                 #faceid value

    if estado == True:
        print("\nEl hombre con más barba es: ",faceId1,
            "con un nivel de barba de:",barba,
            "\ntiene un porcentaje de felicidad de: ",happiness1)
    else:
        print("\nLo siento, no se detectaron hombres en la imagen")     #prints


#===========================================================================================================

#17. (O)
  
def lentes_enojo(rostros):
    '''
    Shows the person who wears glasses and is more angry, 
    if no glasses are detected in any person it is indicated
    Receive: faces list
    Returns: nothing
    '''
    angry1 = 0
    estado= False                                                        # Variables
    faceId1 = " "        

    for rostro in rostros:

        faceId=rostro['faceId']

        faceAttributes = rostro['faceAttributes']
                                                       # cycle to remove glasses and anger
        glasses = faceAttributes['glasses']

        emotion = faceAttributes['emotion']  

        angry=emotion['anger']

        if glasses != 'NoGlasses':
            estado= True

            if angry >= angry1:   
                                                    # get the biggest anger percentage
               angry1 = angry                                  
               faceId1 = faceId    
                                 # faceid value
    if estado == True:
        print("\nLa persona más enojada es",faceId1,
            "Con un porcentaje de enojo de: ",angry1)

    else:
        print("\nLo siento, no se detectaron personas con lentes")        #prints
                                                    
#===========================================================================================================

#18. (P)

def rubia_mujer(rostros):
    '''
    Function that shows the woman with the blonde hair,
    if no women are detected it is indicated.
    Receive: faces list
    Returns: nothing
    '''                                   #Variables
    rubia= 0
    faceId1 = ''
    estado = False

    for rostro in rostros:
        faceId=rostro['faceId']
        faceAttributes = rostro['faceAttributes']
        gender = faceAttributes['gender']           #cycle to get hair color and gender
        hair= faceAttributes['hair']
        hairColor=hair['hairColor']

        if gender == 'male':
            continue

        estado=True
        
        for color in hairColor:
            if color['color']=='blond':

                color1=color['color']
                color2=color['confidence']
                break

        if color2 > rubia:
            rubia=color2
            faceId1=faceId

    if estado == True:
        print("\nLa mujer más rubia es: ",faceId1,
            "\ntiene un porcentaje de: ",rubia)
    else:
        print("\nLo siento, no se detectaron mujeres en la imagen")                 #prints
                                                    
#===========================================================================================================

#19. (M)

def mostrar_felicidad_promedio(rostros):
    '''
    the person with the happiness closest to the average is shown
    it is verified that there is more than one person to take the
    average and that there is happiness below the average
    Receive: faces list
    Returns: nothing
    '''
    if len(rostros) == 1:
        print('\nLo siento, no es posible calcular nungún promedio, '
            'debido a que solo hay una persona en la imagen.')
    else:
        face=list()
        suma = 0  
        feli = 0
        faceId1 = " "           #Variables.
        for rostro in rostros:


            lista=list()  
            faceId=rostro['faceId']
            faceAttributes = rostro['faceAttributes']
            emotion = faceAttributes['emotion']  
            happiness=emotion['happiness']                  #cycle to get happiness and create the lists.
            suma=suma + happiness
            lista.append(happiness)
            lista.append(faceId)
            face.append(lista)
            #print(lista)

        promedio=suma / len(face) 

        if suma == len(face):
            print("No existe ningúna felicidad debajo del promedio.")
        else:
            for i in face:

                if i[0] > feli and i[0] < promedio:
                    feli = i[0]                                      #cycle that scores the highest below average.
                    faceId1 = i[1]
            print("\nLa persona",faceId1,
            "tiene el porcentaje de felicidad más cercano al promedio con: ",feli)     #prints

#===========================================================================================================

#20. (Q1) 

def mostrar_sonrisa(rostros):
    '''
    The person or people with the most akto smile level is shown
    Receive: faces list
    Returns: nothing
    '''
    sonrisa=0
    faceId_real=list()

    for rostro in rostros:
        faceId= rostro['faceId']
        faceAttributes=rostro['faceAttributes']     #smile and face id of each person
        smile=faceAttributes['smile']

        if smile == sonrisa:                #the person's smile level is compared to the current smile
            faceId_real.append(faceId)      #if several people share the same level of smile they only add to one list

        elif smile > sonrisa:

            faceId_real=list()
                                    #if there is a person with a higher smile level, the list of smiles is updated
            sonrisa=smile

            faceId_real.append(faceId)

    if len(faceId_real)==1:
        print('\nel nivel de sonrisa más alto es:',sonrisa,'\nLo tiene la persona:',faceId_real[0])
    else:
        print('\nel nivel de sonrisa más alto es:',sonrisa,'\nLo tienen las personas:',)
        for i in faceId_real:
            print('\t',i)

#===========================================================================================================

#20. (Q2)

def mostrar_bigote(rostros):
    '''
    Shows the person with the biggest moustache
    Receive: faces list
    Returns: nothing
    '''
    bigote=0
    faceId_real=list()

    for rostro in rostros:
        faceId= rostro['faceId']
        faceAttributes=rostro['faceAttributes']     #the smile and faceId is removed from each person
        facialHair=faceAttributes['facialHair']
        moustache=facialHair['moustache']

        if moustache == bigote:             #the person's smile level is compared to the current smile
            faceId_real.append(faceId)      #if several people share the same level of smile they only add to one list

        elif moustache > bigote:

            faceId_real=list()
                                    #if there is a person with a higher smile level, the list of smiles is updated
            bigote=moustache

            faceId_real.append(faceId)

    if len(faceId_real)==1:

        print('\nel nivel de bigote más alto es:',bigote,
            '\nLo tiene la persona:',faceId_real[0])

    else:
        print('\nel nivel de bigote más alto es:',bigote,
            '\nLo tienen las personas:',)

        for i in faceId_real:

            print('\t',i)


#===========================================================================================================

#21. (K)

def show_image(picture,
        rostros):
    '''
    Sample image with rectangle on people's face
    Receive: faces list
    Returns: nothing
    '''

    image = Image.open(picture)

    for rostro in rostros:

        fa = rostro['faceRectangle']

        top = fa['top'] 

        left = fa['left']             #The coordinates are established where it will be drawn

        width = fa['width'] 

        height = fa['height']

        draw = ImageDraw.Draw(image)

        draw.rectangle((left, top, left+width, top+ height), outline= "fuchsia", width = 4) #It was said rectangle

        #font = ImageFont.truetype("c:/Users/bryam/Desktop/semana_8/Arial_Unicode.ttf",30)

        #draw.text((left+10,top-40), 'FaceId '+str(pos), font=font, fill="white")

    image.show(image)       
#=========================================================================================================== 
def historial_consultas(rostros):
    '''
    Function that saves query history
    Receive: faces list
    Returns: historial
    '''
    historial=list()
    for rostro in rostros:
        dicc=dict()

        faceId=rostro['faceId']
        faceAttributes=rostro['faceAttributes']
        gender=faceAttributes['gender']             #Information that must go to history is saved in variables
        age=faceAttributes['age']
        emotion=faceAttributes['emotion']
        faceRectangle=rostro['faceRectangle']

        dicc['faceId']= faceId
        dicc['gender']= gender
        dicc['age']= age                        #Variable information is stored in dictionaries
        dicc['emotion']= emotion
        dicc['faceRectangle']= faceRectangle

        historial.append(dicc)  #Each dictionary is added in a list

    return historial

def joven_emocionnes_rec(picture, rostros):
    '''
    Feature showing younger person with rectangle on face
    Receive: image path, history
    Returns: nothing
    '''
    mas_joven=150
    faceId_real=str()
    emociones= dict()
    rostro_joven=dict()
    for rostro in rostros:

        faceId=rostro['faceId']
        faceRectangle=rostro['faceRectangle']
        emotion=rostro['emotion']
        age=rostro['age']   

        if age < mas_joven:
            mas_joven=age            #The youngest person is taken out
            faceId_real= faceId
            emociones=emotion
            rostro_joven=rostro

    print('\nLa persona más joven es:', faceId_real)

    print('Edad:',mas_joven)

    print('Emociones: ')

    for x, y in emociones.items():

            print('\t',x + ':', y)

    show_image(picture, [rostro_joven])     #The function is called to draw rectangle on the youngest face

def mostrar_prom_emotion(rostros):
    '''
    Shows the average of people's emotions
    Receive: faces list
    Returns: historial
    '''
    emociones=[
    0, 0, 0,
    0, 0, 0,
    0, 0
    ]

    nombre_emo=[
    'anger', 'contempt','disgust',
    'fear', 'happiness', 'neutral',
    'sadness', 'surprise'
    ]

    for rostro in rostros:

        emotion = rostro['emotion']

        emociones[0] = emotion['anger'] 
        + emociones[0]

        emociones[1] = emotion['contempt'] 
        + emociones[1]

        emociones[2] = emotion['disgust'] 
        + emociones[2]                     #The confidence level of each emotion is added to a list

        emociones[3] = emotion['fear'] 
        + emociones[3]

        emociones[4] = emotion['happiness'] 
        + emociones[4]

        emociones[5] = emotion['neutral'] 
        + emociones[5]

        emociones[6] = emotion['sadness'] 
        + emociones[6]

        emociones[7] = emotion['surprise'] 
        + emociones[7]

    rostros_len = len(rostros)

    print('El promedio de las emociones es:')

    for emo in range(len(emociones)):

        emociones[emo] = emociones[emo] / rostros_len #The average is taken

        print('\n\t' 
            + nombre_emo[emo] 
            + ':', round(emociones[emo], 3))

main()
